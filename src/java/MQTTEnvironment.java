// MQTT library: https://repo.eclipse.org/content/repositories/paho-releases/org/eclipse/paho/org.eclipse.paho.client.mqttv3/1.0.2/org.eclipse.paho.client.mqttv3-1.0.2.jar
// Remember to add it to your classpath in Eclipse and in the .mas2j file.

// You also need an MQTT broker. I'm using mosquitto, http://mosquitto.org/

// You can test the whole thing by publishing messages from the command line.

import jason.asSyntax.*;
import jason.environment.*;

import java.util.logging.*;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MQTTEnvironment extends Environment implements MqttCallback {

    private Logger logger = Logger.getLogger("janus_test_logger."+MQTTEnvironment.class.getName());

    private MqttClient client;

    
    /** Called before the MAS execution with the args informed in .mas2j */
    @Override
    public void init(String[] args) {
        super.init(args);
        addPercept(Literal.parseLiteral("percept(demo)"));
        System.out.println("Running!");
        try {
            client = new MqttClient("tcp://localhost:1883", "JasonMQTTEnvironment");
            client.connect();
            MqttMessage message = new MqttMessage();
            message.setPayload("JasonMQTTEnvironment is up and running".getBytes());
            client.publish("JasonMQTTEnvironment/log", message);
            client.setCallback(this);
            client.subscribe("MQTTJason/#"); // # is the wildcard for multiple topics
            // client.disconnect();
          } catch (Exception e) {
            e.printStackTrace();
          }
    }

    @Override
    public boolean executeAction(String agName, Structure action) {
        logger.info("executing: "+action+", but not implemented!");
        return true;
    }

    /** Called before the end of MAS execution */
    @Override
    public void stop() {
        super.stop();
    }
    
    /******* FUNCTIONS TO IMPLEMENT MQTT CALLBACKS ***********/
    

	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// TODO Auto-generated method stub
		
	}

	public void messageArrived(String topic, MqttMessage message) throws Exception {
		System.out.println("-------------------------------------------------");
		System.out.println("| Topic:" + topic);
		System.out.println("| Message: " + new String(message.getPayload()));
		System.out.println("-------------------------------------------------");
		
		// I am assuming a default format for publish messages:
		// MQTTJason/sensorid/temperature [for temperature]
		// MQTTJason/sensorid/humidity [for humidity]
		String[] components = topic.split("/"); 
		if ( components[2].equals("temperature") ) {
			addPercept(Literal.parseLiteral("temp_sensor(" + 
												components[1] +
												"," +
												new String(message.getPayload()) +
												")"));
		} else {
			addPercept(Literal.parseLiteral("humi_sensor(" + 
					components[1] +
					"," +
					new String(message.getPayload()) +
					")"));
		}
		
	}

	public void connectionLost(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

}
