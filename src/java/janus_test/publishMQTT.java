// Internal action code for project janus_test

package janus_test;

import jason.*;
import jason.asSemantics.*;
import jason.asSyntax.*;

public class publishMQTT extends DefaultInternalAction {
	
	

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        // execute the internal action
        ts.getAg().getLogger().info("executing internal action 'janus_test.publishMQTT'");
        if (true) { // just to show how to throw another kind of exception
            throw new JasonException("not implemented!");
        }
        
        // everything ok, so returns true
        return true;
    }
}
