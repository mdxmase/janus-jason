// Agent air conditioner in project janus

/* This agent is just going through a set of states: at the start it is in a wait 
    state. If it receives a message turnon, it will go through states
	set_up, cooling, cooldown, then wait again. Message turnon is only accepted
	if in state waiting. When a state changes, a message is broadcasted (and
	therefore it can be monitored by the controller)
*/

/* Initial beliefs and rules */

/* Initial goals */

waiting.

/* Plans */

+waiting : true
   <- .my_name(Me);
     .broadcast(tell,aircond_msg(Me,waiting));
	 .print("Aircond ready.").
   
+turnon : waiting 
	<- -waiting;
   	   .print("turnon message received, going to setup.");
	   +setup.

+setup : true 
  <- .my_name(Me);
     .broadcast(tell,aircond_msg(Me,setup));
  	 .print("In setup, going to cooling.");
	 .wait(1500);
	 -setup;
	 +cooling.
	 
+cooling : true 
  <- .my_name(Me);
     .broadcast(tell,aircond_msg(Me,cooling));
  	 .print("In cooling, going to cooldown.");
	 .wait(1500);
	 -cooling;
	 +cooldown.
	 
+cooldown : true 
  <- .my_name(Me);
     .broadcast(tell,aircond_msg(Me,cooldown));
  	 .print("In cooldown, going to waiting.");
	 .wait(1500);
	 -cooldown;
	 +waiting.
	