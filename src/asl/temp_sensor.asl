// Agent temp_sensor in project janus


sense(temperature).

/* Initial goals */

!start.

/* Plans */

+!start : true & sense(temperature)
	<- .at("now + 15 s", "+timeToSend");
		!send.

 +!send <- .random(R);
				T = (10*R) + 22;
				.my_name(Me);
			//.print("temp is ", T);
			//mqtt.publish(ControllerId,"SensorId/temperature/",T);
			.broadcast(tell,temp_sensor(Me,T));
			.wait(3000);
			!start.
			
			