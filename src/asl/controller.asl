// Agent controller in project janus

/* Rules:
   - We add a rule for each kind of sensor, using the three possible patterns:
     (1) remain in a range, (2) do not oscillate more frequently than X, 
	 (3) go through a sequence of states.
   - We use rules in the context of plans below to raise a warning.
   - We also print the current state of the system according to the "fidelity
     function" for the whole system, see below.
*/


/* This is a rule to check whether something is wrong with a temperature sensor.
   It is an example of pattern (1) defined above, aka Frechet distance, see
   https://en.wikipedia.org/wiki/Fr%C3%A9chet_distance
   
   The baseline is a periodic function on the hour of the day, going from 0 at midnight to
   1 at noon. This is multiplied by 10, then added to 15, so that the baseline is
   between 15 (midnight) and 25 (noon).
   The valid range is baseline +/- 5 degrees.
   
   IMPORTANT COMPILATION WARNING!
   math.sin doesn't seem to be included in the default Jason. You need to 
   modify a source file and recompile. In particular:
   - open src/jason/asSyntax/directives/FunctionRegister.java
   - around line 24, add:
       import jason.functions.sin;
   - around line 58, add:
       addJasonFunction(sin.class);
   - save and close FunctionRegister.java.
   - At the root of Jason installation, type:
       ant clean
	   ant compile
	   ant jar
	   ant plugin
	 (this will re-compile and re-build more or less everything)
*/

temperature_something_wrong(S,T) :- temp_sensor(S,T) & 
                                    .time(HH,MM,SS) & 
                                    ( (T < ((math.sin(HH/24*3.14)*10+15) - 5 ) )
									  |
									  (T > ((math.sin(HH/24*3.14)*10+15) + 5 ) )
									  ).

							
/* This is an example of pattern (2): too many requests in a given time window.
   As an example, we set 3 messages in less than 10 seconds as a limit.
   When a humidity sensor data point is received, a humidity_data entry is
   created. humidity_data has: Sensor ID, value of humidity, hour, minute,
   second (FIXME: the formula below will not work when crossing midnight...)
   This rule is used in the plan +humi_sensor. The situation is a bit complicated...
   - When a humi_sensor belif arrives from S, I create a humidity_data(S,H,HH,MM,SS),
     where S is the ID of the sensor, H is the humidity, HH is the hour, MM is the minute,
	 and SS are the seconds. See plan +humi_sensor(S,H) : true, which is executed if
	 there are no problems with the rule humidity_something_wrong below.
   - The plan for +humidity_data(S,H,HH,MM,SS) also deletes data older than the bound,
     see the plan +remove_old_humidity_data(_,HH,MM,SS) (and the number 10).
   - Time is converted to seconds from the midnight.
   
*/
humidity_something_wrong(S,H) :- 
                                 .count(humidity_data(S,_,HH2,MM2,SS2),Npoints) & // count how many readings do we have?
								 //DEBUG: .print("N. of points: ",Npoints) &
								 Npoints > 2. // ...and there are more than 2.


/* This is an example of pattern (3): the aircond should go through a fixed
   sequence of states.
   Something is wrong with the aircond if:
   - There was no previous message (i.e., we are starting), but we don't receive "waiting"
   - There was a transition that was not allowed (this is encoded just by listing all the
      possible transitions)
*/
aircond_something_wrong(S,Msg) :- .print("checking aircond rule for ",S," ",Msg) & ( (not last_aircond_msg(S,Msg1)) & not (Msg == waiting)) |
                                 (last_aircond_msg(S,Msg1) &
								   ( (Msg1 = waiting & not (Msg == setup)) |
								     (Msg1 = setup & not (Msg == cooling)) |
									 (Msg1 = cooling & not (Msg == cooldown)) |
									 (Msg1 = cooldown & not (Msg == waiting)) 
								    )
								  ).


/* A rule for fidelity */

fidelity_pattern_temp(S,T,[]) :- temperature_something_wrong(S,T) & .time(HH3,MM3,SS3).

fidelity_pattern_humi(S,H,[]) :- humidity_something_wrong(S,H) & .time(HH4,MM4,SS4).

fidelity_pattern_aircond(S,Msg,[]) :- aircond_something_wrong(S,Msg) & .time(HH5,MM5,SS5).


// FIXME: compute the driftings.
								  
								  
/* Initial goals */

!start.

/* Plans */

+!start : true
  <- .print("Starting controller");
      .wait(500);
      .send(airconditioner,tell,turnon).
      +get_trustworthiness.


/* When data is stored, we remove any old data */
+humidity_data(S,H,HH,MM,SS) : true 
                <- +remove_old_humidity_data(S,HH,MM,SS).

/* This is the plan to remove data older than 10 seconds (change as appropriate) 
   The context is that there is some humidity_data that satisfies the inequality.
*/
+remove_old_humidity_data(_,HH,MM,SS) : humidity_data(S,H2,HH2,MM2,SS2) &
                                        ( (HH2*3600+MM2*60+SS2) < (HH*3600+MM*60+SS-10)) //
                                        <-  -humidity_data(S,H,HH2,MM2,SS2).
										   
/* When a temperature arrives, we check the context to see if it is wrong */
+temp_sensor(S,T) : temperature_something_wrong(S,T) 
  <- .print("Out of bounds temperature of ",S," for temperature ",T).
      
/* If the context above is false (i.e., if everything is OK), we just remove
   the belief and wait for more data.
*/
+temp_sensor(S,T): true
  <- // DEBUG: .print("Current temp by ",S," is ",T);
  -temp_sensor.

/* As above: when humidity data arrives from a sensor, we check if there is
   anything wrong. We also need to keep removing old data, as at some point
   the time window may slide and we go back to normality.
*/
+humi_sensor(S,H) : humidity_something_wrong(S,H)
  <- .print("Too many readings in 10 seconds for ",S);
     +remove_old_humidity_data(S,HH,MM,SS).
	 
/* Humidity data received and there is no problem: we just store it */
+humi_sensor(S,H): true
  <- //DEBUG: .print("Current humidity is ",H);
  .time(HH,MM,SS); // time received
  +humidity_data(S,H,HH,MM,SS); // received something from S, we store it.
  -humi_sensor.
  
  
/* An example of a machine with states: the airconditioner */
+aircond_msg(S,Msg) : aircond_something_wrong(S,Msg)
  <- .print("Something wrong with the sequence of states for aircond ",S).

+aircond_msg(S,Msg) : true
  <- -last_aircond_msg(S,_);
     .print ("received from ",S,": ",Msg);
     +last_aircond_msg(S,Msg).
    
     
 /* Plans for controlling trustworthiness and actions. Actions do nothing, unless bound to the environment class. */
 
 +get_trustworthiness : machine_fidelity(high) & user_fidelity(high)
  	<- .print("Trustworthy System"); //optimal, sustainable working conditions
  		system_trustworthy.
  		
 +get_trustworthiness : ( machine_fidelity(low) | machine_fidelity(medium) )  & ( user_fidelity(high) | user_fidelity(medium) )
  	<- .print("Unstable System"); // reconfigurable working conditions
  		system_unstable.

 +get_trustworthiness : ( machine_fidelity(high) | machine_fidelity(medium) )  &  user_fidelity(low) 
  	<- .print("Unsafe System");  // alarm-rising working conditions
  		system_unsafe.
  		
 +get_trustworthiness : machine_fidelity(low) & user_fidelity(low)
  	<- .print("Untrustworthy System"); //optimal, sustainable working conditions
  		system_untrustworthy.
 
 

 
 
     