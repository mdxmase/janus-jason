// Agent humidity_sensor in project janus


/* Initial beliefs and rules */


//baseline(Humidity) :- Humidity >= 0 & Humidity <= 100.

//broken(Value1, Value2) :- 
  //.at("now + 5 s", Value1-Value2 > 10).
broken(H) :- .random(R) &H = (10*H)+ 70.
sense(humidity).

/* Initial goals */

//!sense(0).	// baseline
//!sense(1) // broken

/* Plans */

/* Initial goals */

!start.

/* Plans */

+!start : true & sense(humidity)
	<- .at("now + 2 s", "+timeToSend");
		!send.

 +!send <- .random(R); 
 			H = (10*R)+ 70;
 			//.print("humi is ", H);
			.my_name(Me);
			.broadcast(tell, humi_sensor(Me,H));
			//mqtt.publish(ControllerId,"SensorId/humidity/", H);
			.wait(3300);
			!!start.
	
